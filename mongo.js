 
var mongoose = require('mongoose') // stating the mongoose node.js library which interacts with the mongo DB

var database = 'api' //the database name is stored in a private variable instead of being 'hard-coded' so it can be replaced using the 'rewire' module. This avoids the need for the unit tests to run against the 'live' database. 
var Schema = mongoose.Schema // creating the schema needed for the mongo to be able to add data // the Schema is mongoose method which allows to import the module and use it

const server = 'mongodb://'+process.env.IP+'/'+database  // the server connections string includes both the database server IP address and the name of the database. 
console.log(server)

mongoose.connect(server) // the mongoose drivers connect to the server and return a connection object. 
const db = mongoose.connection // pending connection to the test database running on localhost


const listSchema = new mongoose.Schema({ // all documents in a 'collection' must adhere to a defined 'schema'.Defining a new schema that includes a mandatory string.
    query: { type: String, required: true }
    //items:  [{type: Array ,required : false}] 
   // description :  [ {type : Array ,required : false} ]
    
})

const List = mongoose.model('List', listSchema); // the schema is associated with the 'List' collection which means it will be applied to all documents added to the collection. 

module.export = List;



exports.addList = (data, callback) => { // creating a function, which can be used in other modules , which taken 2 parameters : data and callback
  const query = data // data collected from the guitarparty module to add  to db
  //const step1 = query.split('}')
  //console.log(step1)
  
  /* here we use the 'map' function to loop through the array and apply the trim() function. There are several useful functions that are part of the 'array prototype' such as map(), filter(), reduce(). */
  //const items = step1[0].split('}').map(function(item) {
  //  return item.trim()
  //})
  //const description = step1[1].split(',').map(function(it) {
  //  return it.trim()
  //})
  
  
  const newList = new List({query}) // creating new list that adopts the schema stated above, the list adds the data from the guitarparty module
  newList.save( (err, data) => {  // function to save the new list with the data , which returns the callback of error/success
    
    if (err) {
      callback('error: '+err)
    }else
    callback('added: '+data)
    
  })
  
  
}

exports.getAll = callback => { // function to return all the lists from the local Mongo DB
  
  List.find( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    const list = data.map( item => { // here is the function which maps the data(items) from the guitarparty and returns it in the song object
      
      return { song: item.query}
    })
    callback(list) // callback returns the list with added data
  })
}

exports.getById = (id,callback) => { // function to find data by specifying the ID of the added list
  
  List.find({_id:id}, (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    callback(data) // callback returns the data
  })
}
exports.getById1 = (name,callback) => { // function to find data by specifying the name of the added list
  
  List.find({name:name}, (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    callback(data) //callback returns data
  })
}




exports.clear = (callback) => { //function to remove all the lists from the local database
  
  List.remove({}, err => { // removes the object , which had all the data from the database
    if (err) {
      callback('error: '+err)
    }
    callback('lists removed') // callback should return the following message 
  })
}

exports.names  = callback => { // function to return the names of the current lists in the databse
  
  List.find( (err, data) => { 
    if (err) {
      callback('error: '+err)
    }
    const list = data.map( item => {  // mapping the data to return on the name of the list, not the list data
       return item.name
      
    })
    callback(list) //returns the name of the current lists
  })
}