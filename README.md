This is my API for 305CDE

The purpose of this file is to explain the code , written in this repository.
As this is an API,which uses another third-party API , first of all the choice of API was important. Some of the APIs request to have keys to pass the 
authentication process, in my case I had to add my key in my guitarparty.js file and put it in the 

"request.get({headers: {  // the GET method , which pulls the data in this case has the injected header with my API key, otherwise I wont be able to access the data
               'Guitarparty-Api-Key': 'd0ba136ea0e7bade8e61096631e665b2a0727951'},qs:query_string,url:url}, (err, res, body)"
It is important for the header to be exactly as the third-party API requests, in my case it is the 'Guitarparty-Api-Key' and following the key

Next important thing was to check which query the third-party API uses as this was crucial to actually pulling the data, this was the 

"qs:query_string" which was equal to "const query_string = {query:query }" query had to equal query,not query:q (for example). 

All this information should be available from the url , which was used in the get request. The further information can be found on the Guitar-party api website.

Next:
var fs = require('fs') // stating my required file system nodejs as most of the code depends on this
var restify = require('restify') // another dependency needed to be able to create a local server for my API and run it to use database or interact with the client
var server = restify.createServer() //creating a server to run my API
var mongo = require('./mongo.js') //requiring my database module , as I will be saving data I get from the REST principals
const auth = require('./auth')  // the needed authentication module for my advanced authentication mechanism, which allows me to create new users and authenticate them to be able to store data in the mongo db

To create the server for the API to run :
var port = process.env.PORT || 8080;  //create a local port for the API to work
server.listen(port, function (err) {  //uses restify to listen to this port and perform all the needed actions
  if (err) {     // in case of error
      console.error(err);  // prints out the error
  } else {  // if no error
    console.log('App is ready at : ' + port);  //states the succesful working port at given port
  }
})


It was crucial to tell the files to talk to each other through the 'require' linking. Although some of these are not files but node.js libraries/modules
which had to be installed on the C9 before proceeding. All the installed libraries can be seen in the package.json file in the dependencies section. 

The scripts in the package.json section are mostly the tests/coverage reports , which were used in my API.
Example : "authtest":"jasmine-node authtest --color --verbose --autotest --watch" , authtest is the name to run in the terminal ( npm run authtest ),
then the script follows which specifies the folder where the testing file is. The tests have to be xxx-spec.js ,otherwise they wont run.

For the coverage report it was easier to type "istanbul cover foldername/filename-spec.js" 


1)to get data in POSTMAN have to use guitarparty.search which is located in guitarparty.js file:

var apikey = 'd0ba136ea0e7bade8e61096631e665b2a0727951' //guitarparty api needed to surpass the third-party authentication mechanism / this will go into the header in my guitarparty module

server.get('/songs/', function(req, res) { // main method of requesting the data from the third-party api ,here I state the route/collection where I will be storing/using the data , and functions to handle the GET request
  
  const searchTerm = req.query.query // stating the name of search query as different third-party APIs ask for different searchterms
  console.log('q='+searchTerm) // checking that the search mechanism returns the valid search 
  guitarparty.search(searchTerm, function(data) { // using export methods to talk to the third-party API in my guitarparty module
    const stuff = data.response.data  // creating a variable to store the received data from the third-party API to futher manipulate it
    
  auth.getAccount(req, (err, data) => {  // using the authentication module to process the validation of data / only authenticated users can store data in the mongo db / passing the request object to an async function that takes callback
    res.setHeader('content-type', 'application/json')  // setting header with appropriate parameters ( will return json data)
    if (err) {
     
      res.send(400, {status: 'error', message: err.message + ' /no authentication, data wasnt added to database',data:stuff }) // if no authentication header found , it count as error/bad request , however still returns data , but doesnt add to the database
      
    } else {  // if valid user found
      console.log(data)
      res.setHeader('content-type', 'application/json');
      
      res.send(200, {status: 'success', message: 'authentication passed, data below added to database', data: stuff})  // sending successful 200 response and adding the data to the mongo db
      
       mongo.addList(JSON.stringify(stuff), data => {  // if user authenticated , the displayed data gets stored in the mongo database, stringify used for the storing arrays rather than objects
        console.log('returned: '+data)
        
    })
     
      
    }
    })
    
  })
}) */

Explanation of 1): Here you can see my GET method SHOW THE EXACT CODE, which is handling the data from the third-party API. My API uses GuitarParty API to find names of the songs with detailed data. The GET method  states in which collection I will be fetching the data to, and has query : the search parameter and response, which  will handle the data.
Next my guitarparty module states what song I was searching for (search parameter) and the function to return data,which is stored in variable called ‘Stuff’. 
Next I have my authorization method which checks if the GET request had a valid basic authentication header. I set the data that will be returned to be of type json. If error ,the response will return the 400 status code ,state no authentication ,which means the data wasn’t added to database, but is available to see in POSTMAN. If the user was authenticated the GET request returns the data and adds it to the mongo database.

2)Guitarparty.js handles the fetching of data and maps it ( only specified data returns ). Callback is needed to return data when the functions fired.

var request = require('request') // stating my request module to be able to make simple http calls and return data

exports.search = (query, callback) => {  // my main search functions handling the query (my search parameter to be able to fetch data) and callback to return the data
  console.log('search')
  if (typeof query !== 'string' || query.length === 0) {  // simple validation of the query , which is absoloutely needed to fetch data/ if its not a string or less than 1 character
    callback({code:400, response:{status:'error', message:'missing query (q parameter)'}}) // my callback handles the bad response status + states no valid query parameter
  }
  
  const url = 'http://api.guitarparty.com/v2/songs/'          //  GuitarParty Api url , which is crucial to fetch the data , as the collection of songs is kept in the 'songs' collection
  const query_string = {query:query }  // stating the query string , important to use the one the third-party API asks to use / otherwise no data returns  
  request.get({headers: {  // the GET method , which pulls the data in this case has the injected header with my API key, otherwise I wont be able to access the data
               'Guitarparty-Api-Key': 'd0ba136ea0e7bade8e61096631e665b2a0727951'},qs:query_string,url:url}, (err, res, body) => { // stating the other key parameters to process the GET request as well as the passing the request object to an async function that takes a callback.
    if (err) {
      callback({code:500, response:{status:'error', message:'search failed', data:err}}) // returns error status code and states the error
    }
    console.log(typeof body) // checking the format in which the data came 
    const json = JSON.parse(body) // converting string to the Object 
    const items = json.items // applying the convertion to the items , the data which gets returned is stored in the items object
    
    const songs = json.objects.map(element => { // mapping the data / dont want to see it all , the code maps the data and I can return the only bits I want
      return {'ID':element.id,'Link':element.permalink,'Authors':element.authors[0].name,'Title':element.title , 'Instrument':element.chords[0].instrument.name,'Tuning':element.chords[0].instrument.tuning} 
      
    })
    console.log(songs)
    callback({code:200, response:{status:'success', message:songs.length+' songs found', data:songs}}) // finally the callback which returns the response status and the mapped data I requested
  })
}

Explanation of 2) :Next my guitarparty module states what song I was searching for (search parameter) and the function to return data,which is stored in variable called ‘Stuff’. Next I have my authorization method which checks if the GET request had a valid basic authentication header. I set the data that will be returned to be of type json. If error ,the response will return the 400 status code ,state no authentication ,which means the data wasn’t added to database, but is available to see in POSTMAN. If the user was authenticated the GET request returns the data and adds it to the mongo database. 

These were the main files needed to make the API useful 

The mongo.js file is needed to create schema ( how data will be stored ) and save List function, to be able to save the data.
First need to install mongoose and create/ run the server locally for mongo db to work.

3)
var mongoose = require('mongoose') // stating the mongoose node.js library which interacts with the mongo DB
var database = 'api' //the database name is stored in a private variable instead of being 'hard-coded' so it can be replaced using the 'rewire' module. This avoids the need for the unit tests to run against the 'live' database. 
var Schema = mongoose.Schema // creating the schema needed for the mongo to be able to add data // the Schema is mongoose method which allows to import the module and use it

const server = 'mongodb://'+process.env.IP+'/'+database  // the server connections string includes both the database server IP address and the name of the database. 
console.log(server)

mongoose.connect(server) // the mongoose drivers connect to the server and return a connection object. 
const db = mongoose.connection // pending connection to the test database running on localhost
const listSchema = new mongoose.Schema({ // all documents in a 'collection' must adhere to a defined 'schema'.Defining a new schema that includes a mandatory string.
    query: { type: String, required: true }
    
})

const List = mongoose.model('List', listSchema); // the schema is associated with the 'List' collection which means it will be applied to all documents added to the collection. 

module.export = List;


exports.addList = (data, callback) => { // creating a function, which can be used in other modules , which taken 2 parameters : data and callback
  const query = data // data collected from the guitarparty module to add  to db
  
  
  const newList = new List({query}) // creating new list that adopts the schema stated above, the list adds the data from the guitarparty module
  newList.save( (err, data) => {  // function to save the new list with the data , which returns the callback of error/success
    
    if (err) {
      callback('error: '+err)
    }else
    callback('added: '+data)
    
  })
  
  
}

Explanation of 3) :Moving on to my Mongo database file(SHOW MONGO FILE), where I had to state the node.js mongoose library to control the database. Here I create the local server and connect it to the mongo database. Next I state a schema , the way data will be added. As you see I only have one parameter , which query of type string, as mongo doesn’t like objects. To store the data I have to create a list with the schema associated with the 'List' collection which means it will be applied to all documents added to the collection. I export the new List as I will have to use it to add the data fetched from the third-party API. 
Then I have some functions which are handy to quickly remove or find desired data stored in the mongo database.

4) The authentication process


var bcrypt = require('bcrypt')  // stating the encryption node.js module to save password in a protected way
var storage = require('node-persist') // stating basic data structure to check for valid accounts
storage.initSync() // syncing the storage

// create an account 
exports.addAccount = (request, callback) => { // function uses request to validate the data, callback to return the data validation
	console.log(request.authorization)
	
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') { // if the request does not have a basic authentication header or is missing the scheme
		callback(new Error('basic authorization header missing')) // callback returns an error
	}
	const username = request.authorization.basic.username // state the username which will be validated 
	
	if (storage.getItemSync(username) !== undefined) { // if an account with the same username exists
		callback(new Error('account '+username+' already exists')) // return error
	}
	const password = request.authorization.basic.password // state the password to be validated
	
	const salt = bcrypt.genSaltSync(10) // creating a const variable to set it to function, which encrypts the data
	
	const hash = bcrypt.hashSync(password, salt) // encrypting the password by using the 'salt' const variable
	/* we store the account details. */
	const account = {username: username, hash: hash} // create the account , where username equals to username and hash is the encrypted password
	storage.setItem(username, account) // adding the account to the storage
	
	
	callback(null, {message: 'account created', username: username} ) // passing the username account back to the routes file
}

// check if the following account exists
exports.getAccount = (request, callback) => {
	console.log(request.authorization)
	
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') {  // if the request does not have a basic authentication header or is missing the scheme
		callback(new Error('basic authorization header missing'))   // callback returns an error
	}
	const username = request.authorization.basic.username // state the username which will be validated
	
	if (storage.getItemSync(username) === undefined) { // if an account with the  username doesnt exist
		callback(new Error('account '+username+' does not exist exists')) // callback returns an error
	}
	var account = storage.getItemSync(username) // getting the username account from the storage
	const password = request.authorization.basic.password // state the password to be validated
	console.log(password+' : '+account.hash)
	
	if (!bcrypt.compareSync(password, account.hash)) { // comparing the supplied password to see if its not valid
		callback(new Error('invalid password')) // returns error , wrong password
	}
	const data = {username: username} // data equals to the username account , if validation complete , the accounts match
	callback(null, data) // returns the existing  account 
}
explanation of 4) :My module which handles the authentication requires to state  storage node persist filesystem(SHOW WHATS STORED IN THE PERSIST FOLDER). I also state the function algorithm which will encrypt my password. There are only 2 main functions which control the authentication and have almost the same code. The GetAccount function (SHOW THE FUNCTION)has two parameters : request , which handles the main authentication process and callback to return the data. If the request doesn’t have any basic authorization then the error will explain that.  Next the username and passwords are created to check if they exist. If the username doesn’t match the ones that are stored in the storage, error will be returned. If the password doesn’t match username’s hash stored password, the error will be returned. Finally if the username match and their passwords match, success, the POSTman will show the username is logged in. It is vital for my API to be ‘logged in’ as it is the only way when the data gets stored in the mongo database. 
The AddAccount function (SHOW THE FUNCTION)has the same conditions to return error : if username already exists or if no basic authorization is passed with the request. If everything is correct ,the typed in username and password will be stored in the storage where the GetAccount function validates it.

Testing : Hardest part so far , hard to get the syntax at first, tests are easy to write though.
Example : 
var bcrypt = require('bcrypt')  // stating the encryption node.js module to save password in a protected way
var storage = require('node-persist')  // stating basic data structure to check for valid accounts
var auth = require('../auth.js')  // stating the directory of the file that is being tested
describe('Authentication',  () => {  // have to add describe as this states what the tests are gonna test and pass the function ( not always empty one)
	it('should clear the storage', done => { this is where the test actually is coded. Done is important as without it the tests will not end running and wont return the results.
	    storage.clearSync() // checking if after clearing the storage 
	        expect(storage.values()).toEqual([]) // it is empty // expect is where something  that must happen has to be equal to the what will happen.
	        done(); // finishes the test and brings back the results.
	});
})