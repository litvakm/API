var bcrypt = require('bcrypt')  // stating the encryption node.js module to save password in a protected way
var storage = require('node-persist')  // stating basic data structure to check for valid accounts
var auth = require('../auth.js')  // stating the directory of the file that is being tested

describe('Authentication',  () => {  

	it('should clear the storage', done => {
	    storage.clearSync() // checking if after clearing the storage 
	        
	        expect(storage.values()).toEqual([]) // it is empty
	        done();
	    
	});
})
describe('get username back',  () => {
    
	it('get user', done => { // using mock data to check the functionality of the account module
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(username, account)
	    
	    expect(storage.getItemSync(username)).not.toEqual(undefined) // simple test to check if the storage did add the new account and wont return undefined
	       
	        done();
	    });
	});

describe('get password back',  () => {
    
	it('get password', done => { // using mock data to check if the password was saved correctly
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(password, account)
	    
	    expect(storage.getItemSync(password)).not.toEqual(undefined) // checking if password is not undefined
	       
	        done();
	    });
	});
describe('get wrong password/testing bad data',  () => {
    
	it('get password', done => { // case of bad data , checking if the password wont equal to username
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(password, account)
	    
	    expect(storage.getItemSync(password)).not.toEqual('mike') // password should not be 'mike'
	       
	        done();
	    });
	});
describe('get wrong username',  () => {
    
	it('get username', done => {
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(username, account)
	    
	    expect(storage.getItemSync(username)).not.toEqual('mik') // username should not be qual to 'mik' , missing 'e' for the correct username
	       
	        done();
	    });
	});

   


	

	
	
	