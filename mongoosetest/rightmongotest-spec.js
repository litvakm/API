//var mongoose = require('mongoose');
var mongo = require("../mongo.js") // specifying the location of the module being tested
var List = require("../mongo.js") // importing the function to test

describe('Mongoose list',  () => { // describe the test

	it('should clear all the lists and return "lists removed"', done => { // stating what this test should do, the 'done' parameter is very important as without it the test wont return the result
	    mongo.clear(result =>{ // using the mongo functions to test functionality
	        expect(result).toEqual('lists removed') // expecting the callback of the function to equal to 'lists removed'
	        done(); // important to add this line to get test results back
	    });
	});    
	
	
	it('should find all lists',done =>{
	   mongo.getAll(result =>{
	      expect(result).toEqual([]) 
	      done();
	   }); 
	});
});
var request = require("request"); // stating my request module to be able to make simple http calls and return data

//var base_url = "http://www.myapifilms.com/imdb?title=day"
var base_url = "https://api-mchael.c9users.io/songs?query=slipknot" // using my running API to test the data + mongo functionality

describe("Status code", ()=> {
  describe("Test connection", () => {
    it("returns status code 400", done => {
      request.get(base_url, (error, response, body) =>{ // making a simple request to test the functionality
        expect(response.statusCode).toBe(400); // expecting the GET request to return data error / missing authorisation header ( response to be 400)
        done();
      });
    });

    it("adds mock data to mongo and returns it", done =>{
      
          request.get(base_url, (error, response, body) => {

            const jsona = JSON.parse(body) 
            const items = jsona.items
            console.log(jsona)
            expect(body).toEqual(body); // simple test to make sure the body is valid
            done();
            mongo.addList(jsona,jsona =>{
            });
            
            mongo.getAll(result=>{
	        expect(result).toEqual([ ]) // after adding the data to mongo, expecting the database  be empty / as no autherisation was done
	        console.log(result)
	        done();
	   }); 
            
      });
    });
    it("find by ID", done => { //get this workin
      
          request.get(base_url, (error, response, body) => {

            const jsona = JSON.parse(body)
            const items = jsona.items
            console.log(jsona)
            //expect(body).toEqual(body);
            done();
            mongo.addList(jsona,jsona =>{ // adding data to database
            });
            
            mongo.getById(List,result =>{ // searching for the right ID
	        expect(result).not.toEqual([])  // expecting the search not to return empty array
	        //console.log(result)
	        done();
	   }); 
            
      });
    });
    
    
  });
});


