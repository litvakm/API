var request = require('request'); // stating my request module to be able to make simple http calls and return data
var base_url = "https://api-mchael.c9users.io/songs?query=slipknot"  // stating my APIs URL
var guitarparty = require('../guitarparty.js')
// the tests written below share the same structure and purpose and wont be commented as they pretty much are the same
describe("Test connection", () => {
    it("returns status code 400", done => {
      request.get(base_url, (error, response, body) =>{ // making a simple request to test the functionality
        expect(response.statusCode).toBe(400); // expecting the GET request to return data error / missing authorisation header ( response to be 400)
        done();
      });
    });
    it("returns status code not 200", done => {
      request.get(base_url, (error, response, body) =>{ // making a simple request to test the functionality
        expect(response.statusCode).not.toBe(200); // expecting the GET request to return data error / missing authorisation header ( response to be 400)
        done();
      });
    });
})
describe("FIND that the search returns the right element", () => {    //use this for others/bad data
    it("find right element (LINK)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body) // changing string to object
                  //var items = json.items
                  var songs = json.data.map((element) => { // mapping the data to return only the desired element
                  return {'Link':element.Link} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { Link : 'http://www.guitarparty.com/song/snuff/' } ]) // expecting the results to match
              done();
              });
    });
    it("find right element (ID)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body)
                  //var items = json.items
                  var songs = json.data.map((element) => {
                  return {'ID':element.ID} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { ID: 2766 } ])
              done();
              });
    });
    it("find right element (AUTHORS)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body)
                  //var items = json.items
                  var songs = json.data.map((element) => {
                  return {'Authors':element.Authors} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { Authors: 'Slipknot' } ])
              done();
              });
    });
    it("find right element (TITLE)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body)
                  //var items = json.items
                  var songs = json.data.map((element) => {
                  return {'Title':element.Title} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { Title: 'Snuff' } ])
              done();
              });
    });
    it("find right element (INSTRUMENT)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body)
                  //var items = json.items
                  var songs = json.data.map((element) => {
                  return {'Instrument':element.Instrument} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { Instrument: 'Guitar' } ])
              done();
              });
    });
    it("find right element (TUNING)",done =>{
              request.get(base_url, (error, response, body) => {
                  var json = JSON.parse(body)
                  //var items = json.items
                  var songs = json.data.map((element) => {
                  return {'Tuning':element.Tuning} 
                    //console.log("THESE SONGS: "+songs)
                  })
              
              expect(songs).toEqual([ { Tuning: 'EADGBE' } ])
              done();
              });
    });

});