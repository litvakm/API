
var bcrypt = require('bcrypt')  // stating the encryption node.js module to save password in a protected way
var storage = require('node-persist') // stating basic data structure to check for valid accounts
//var localStorage = require('node-persist') // stating the localstorage database to store the created accounts
storage.initSync() // syncing the storage

// create an account 
exports.addAccount = (request, callback) => { // function uses request to validate the data, callback to return the data validation
	console.log(request.authorization)
	
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') { // if the request does not have a basic authentication header or is missing the scheme
		callback(new Error('basic authorization header missing')) // callback returns an error
	}
	const username = request.authorization.basic.username // state the username which will be validated 
	
	if (storage.getItemSync(username) !== undefined) { // if an account with the same username exists
		callback(new Error('account '+username+' already exists')) // return error
	}
	const password = request.authorization.basic.password // state the password to be validated
	
	const salt = bcrypt.genSaltSync(10) // creating a const variable to set it to function, which encrypts the data
	
	const hash = bcrypt.hashSync(password, salt) // encrypting the password by using the 'salt' const variable
	/* we store the account details. */
	const account = {username: username, hash: hash} // create the account , where username equals to username and hash is the encrypted password
	storage.setItem(username, account) // adding the account to the storage
	//localStorage.setItem(username,account) // adding the account to the localStorage
	//console.log(localStorage.getItem(username,account)) 
	
	callback(null, {message: 'account created', username: username} ) // passing the username account back to the routes file
}

// check if the following account exists
exports.getAccount = (request, callback) => {
	console.log(request.authorization)
	
	if (request.authorization == undefined || request.authorization.scheme !== 'Basic') {  // if the request does not have a basic authentication header or is missing the scheme
		callback(new Error('basic authorization header missing'))   // callback returns an error
	}
	const username = request.authorization.basic.username // state the username which will be validated
	
	if (storage.getItemSync(username) === undefined) { // if an account with the  username doesnt exist
		callback(new Error('account '+username+' does not exist exists')) // callback returns an error
	}
	var account = storage.getItemSync(username) // getting the username account from the storage
	const password = request.authorization.basic.password // state the password to be validated
	console.log(password+' : '+account.hash)
	
	if (!bcrypt.compareSync(password, account.hash)) { // comparing the supplied password to see if its not valid
		callback(new Error('invalid password')) // returns error , wrong password
	}
	const data = {username: username} // data equals to the username account , if validation complete , the accounts match
	callback(null, data) // returns the existing  account 
}
