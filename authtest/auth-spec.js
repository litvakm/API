var bcrypt = require('bcrypt')
var storage = require('node-persist')
var auth = require('../auth.js')

describe('Authentication',  () => {

	it('should clear the storage', done => {
	    storage.clearSync()
	        
	        expect(storage.values()).toEqual([])
	        done();
	    
	});
})
describe('get username back',  () => {
    
	it('get user', done => {
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(username, account)
	    
	    expect(storage.getItemSync(username)).not.toEqual(undefined)
	       
	        done();
	    });
	});

describe('get password back',  () => {
    
	it('get password', done => {
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(password, account)
	    
	    expect(storage.getItemSync(password)).not.toEqual(undefined)
	       
	        done();
	    });
	});
describe('get wrong password/testing bad data',  () => {
    
	it('get password', done => {
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(password, account)
	    
	    expect(storage.getItemSync(password)).not.toEqual('mike')
	       
	        done();
	    });
	});
describe('get wrong username',  () => {
    
	it('get username', done => {
	    var username = 'mike'
	    var password = 'this'
	    var salt = bcrypt.genSaltSync(10)
        const hash = bcrypt.hashSync(password, salt)
	
	    const account = {username: username, hash: hash}
        storage.setItem(username, account)
	    
	    expect(storage.getItemSync(password)).not.toEqual('mik')
	       
	        done();
	    });
	});

   


	

	
	
	