
//Main js file for my API , here I have uncluded all the needed dependencies/restify modules and have raw code to my REST principles, which allow me to perform
// GET / POST methods 

var fs = require('fs') // stating my required file system nodejs as most of the code depends on this

var restify = require('restify') // another dependency needed to be able to create a local server for my API and run it to use database or interact with the client
var server = restify.createServer() //creating a server to run my API
var mongo = require('./mongo.js') //requiring my database module , as I will be saving data I get from the REST principals
const auth = require('./auth')  // the needed authentication module for my advanced authentication mechanism, which allows me to create new users and authenticate them to be able to store data in the mongo db



// here I have some restify plugins , which allow me to use handlers to provide more control over my API
server.use(restify.fullResponse()) // allows to receive all the data collected back
server.use(restify.queryParser()) //query parsing
server.use(restify.bodyParser()) //body parsing
server.use(restify.authorizationParser()) //authentication parsing

var imdb = require('./imdb.js') // module for movies API
var guitarparty = require('./guitarparty.js') // module for Guitarparty API

const stdin = process.openStdin()

stdin.on('data', chunk => {
  console.log(typeof chunk)
  var text = chunk.toString().trim()
  console.log(typeof text)
  
  if (text.indexOf('add ') === 0) {
    var space = text.indexOf(' ')
    var item = text.substring(space+1).trim()
    console.log('adding "'+item+'"')
    
    mongo.addList(item, data => {
        console.log('returned: '+data)
    })
  }
  
  if (text.indexOf('get ') === 0) {
    var space = text.indexOf(' ')
    var item = text.substring(space+1).trim()
    console.log('finding: ID "'+item+'"')
    if (isNaN(item[0])){
    mongo.getById1(item, data => {
        console.log(data)
    })}else{
    mongo.getById(item, data => {
        console.log(data)
    })}
  }
  
  if (text.indexOf('list') === 0) {
    mongo.getAll( data => {
        console.log(data)
    })
  }
  
  if (text.indexOf('clear') === 0) {
    mongo.clear( data => {
        console.log(data)
    })
  }
  if(text.indexOf('names')===0){
    mongo.names(data =>{
      console.log(data)
    })
  }
})

// this works for songs api
var apikey = 'd0ba136ea0e7bade8e61096631e665b2a0727951' //guitarparty api

server.get('/songs/', (req, res) => { 
  console.log('GET /library')
  const searchTerm = req.query.query
  console.log('q='+searchTerm)
  
  guitarparty.search(searchTerm, data => {
    const stuff = data.response.data
    
    console.log(data)
    res.setHeader('content-type', 'application/json');
    res.send(data.code, data.response);
    res.end();
  })
  
})  

/* Used for authentication but doesnt work with client
var apikey = 'd0ba136ea0e7bade8e61096631e665b2a0727951' //guitarparty api needed to surpass the third-party authentication mechanism / this will go into the header in my guitarparty module

server.get('/songs/', function(req, res) { // main method of requesting the data from the third-party api ,here I state the route/collection where I will be storing/using the data , and functions to handle the GET request
  
  const searchTerm = req.query.query // stating the name of search query as different third-party APIs ask for different searchterms
  console.log('q='+searchTerm) // checking that the search mechanism returns the valid search 
  guitarparty.search(searchTerm, function(data) { // using export methods to talk to the third-party API in my guitarparty module
    const stuff = data.response.data  // creating a variable to store the received data from the third-party API to futher manipulate it
    
  auth.getAccount(req, (err, data) => {  // using the authentication module to process the validation of data / only authenticated users can store data in the mongo db / passing the request object to an async function that takes callback
    res.setHeader('content-type', 'application/json')  // setting header with appropriate parameters ( will return json data)
    if (err) {
     
      res.send(400, {status: 'error', message: err.message + ' /no authentication, data wasnt added to database',data:stuff }) // if no authentication header found , it count as error/bad request , however still returns data , but doesnt add to the database
      
    } else {  // if valid user found
      console.log(data)
      res.setHeader('content-type', 'application/json');
      
      res.send(200, {status: 'success', message: 'authentication passed, data below added to database', data: stuff})  // sending successful 200 response and adding the data to the mongo db
      
       mongo.addList(JSON.stringify(stuff), data => {  // if user authenticated , the displayed data gets stored in the mongo database, stringify used for the storing arrays rather than objects
        console.log('returned: '+data)
        
    })
     
      
    }
    })
    
  })
}) */
// using the same principals as for the guitarparty API , same code
server.get('/movies', (req, res) => {
  console.log('GET /library')
  const searchTerm = req.query.title
  console.log('q='+searchTerm)
  imdb.search(searchTerm, data => {
    console.log(data)
    res.setHeader('content-type', 'application/json');
    res.send(data.code, data.response);
    res.end();
  })
})


server.post('/accounts', (req, res) => {
  
  auth.addAccount(req, (err, data) => {  // passing the request object to an async function that takes a callback. The first callback parameter is an Error object.Second paramter is data which gets returned
    
    res.setHeader('content-type', 'application/json') // returns json data
    if (err) {
      
      res.send(400, {status: 'error', message: err.message }) // send back the error information stating the error status and the error itself
    } else {
      
      res.send(201, {status: 'success', message: 'account created', data: data})  // if no errors , the data returned is followed with the 201 code (created/success), following the message and the data itself
    }
  })
})
//This GET method is called on the accounts collection which validates if the user exists or not //
// the code is not much different from the method above and I wont comment the same lines of code twice
server.get('/accounts', (req, res) => {
  
  auth.getAccount(req, (err, data) => {
    res.setHeader('content-type', 'application/json')
    if (err) {
      res.send(401, {status: 'error', message: err.message })
    } else {
      res.send(200, {status: 'success', message: 'account authenticated', data: data}) // 200 status response when the user *logs in* ( seen in Postman)
      // followed by the message and the data , which in my case is the username name
    }
  })
})
var port = process.env.PORT || 8080;  //create a local port for the API to work
server.listen(port, function (err) {  //uses restify to listen to this port and perform all the needed actions
  if (err) {     // in case of error
      console.error(err);  // prints out the error
  } else {  // if no error
    console.log('App is ready at : ' + port);  //states the succesful working port at given port
  }
})


