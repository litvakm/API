//This is my guitarparty API module, my main base code of the API I want to use


var request = require('request') // stating my request module to be able to make simple http calls and return data

exports.search = (query, callback) => {  // my main search functions handling the query (my search parameter to be able to fetch data) and callback to return the data
  console.log('search')
  if (typeof query !== 'string' || query.length === 0) {  // simple validation of the query , which is absoloutely needed to fetch data/ if its not a string or less than 1 character
    callback({code:400, response:{status:'error', message:'missing query (q parameter)'}}) // my callback handles the bad response status + states no valid query parameter
  }
  
  const url = 'http://api.guitarparty.com/v2/songs/'          //  GuitarParty Api url , which is crucial to fetch the data , as the collection of songs is kept in the 'songs' collection
  const query_string = {query:query }  // stating the query string , important to use the one the third-party API asks to use / otherwise no data returns  
  request.get({headers: {  // the GET method , which pulls the data in this case has the injected header with my API key, otherwise I wont be able to access the data
               'Guitarparty-Api-Key': 'd0ba136ea0e7bade8e61096631e665b2a0727951'},qs:query_string,url:url}, (err, res, body) => { // stating the other key parameters to process the GET request as well as the passing the request object to an async function that takes a callback.
    if (err) {
      callback({code:500, response:{status:'error', message:'search failed', data:err}}) // returns error status code and states the error
    }
    console.log(typeof body) // checking the format in which the data came 
    const json = JSON.parse(body) // converting string to the Object 
    const items = json.items // applying the convertion to the items , the data which gets returned is stored in the items object
    
    const songs = json.objects.map(element => { // mapping the data / dont want to see it all , the code maps the data and I can return the only bits I want
      return {'ID':element.id,'Link':element.permalink,'Authors':element.authors[0].name,'Title':element.title , 'Instrument':element.chords[0].instrument.name,'Tuning':element.chords[0].instrument.tuning} 
      
    })
    console.log(songs)
    callback({code:200, response:{status:'success', message:songs.length+' songs found', data:songs}}) // finally the callback which returns the response status and the mapped data I requested
  })
}
